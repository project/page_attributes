CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Page Attributes is a simple module that allow you to customize a body class or
an article class.

It will add a new area at the Advanced section of an add/new node page, and at
the view page, you will have the informed classes.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/page_attributes

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/page_attributes


REQUIREMENTS
------------

 * This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no Configuration settings.


MAINTAINERS
-------------

 * Alessandro Feijó (afeijo) - https://www.drupal.org/u/afeijo
 * Cristiano Demari (cristianodemari) - https://www.drupal.org/u/cristianodemari
